const {createServer} = require('http')
const {pick} = require('underscore')

const server = createServer(function (req, res) {
  const data = pick(req, 'url', 'method', 'headers')
  function send() {
    res.end(JSON.stringify(data))
  }
  if (req.headers['content-length']) {
    const chunks = []
    req.on('data', function (chunk) {
      chunks.push(chunk)
    })
    req.on('end', function () {
      const string = Buffer.concat(chunks).toString('utf8')
      try {
        data.body = JSON.parse(string)
      }
      catch (ex) {
        data.body = string
      }
      send()
    })
  }
  else {
    send()
  }
})

server.listen(8080)
