const {stringify} = require('querystring')
const {request} = require('https')
const {pick, extend, isObject, each, last} = require('underscore')
const merge = require('deepmerge')

function multi({threads = 1, tasks, success, error, delay = 0}) {
  return new Promise(function (resolve, reject) {
    let i = 0

    function iterate() {
      if (i < tasks.length) {
        tasks[i++]()
          .then(function (result) {
            if ('function' === typeof success) {
              success(result)
            }
            iterate()
          })
          .catch(function (err) {
            if ('function' === typeof error) {
              error(err)
            }
            iterate()
          })
          .catch(reject)
      }
      else {
        resolve()
      }
    }

    for (let j = 0; j < threads; j++) {
      setTimeout(iterate, j * delay)
    }
  })
}

class Bot {
  constructor(options) {
    options = pick(options, 'token', 'timeout', 'offset')
    extend(this, merge({
        timeout: 90,
        offset: 0,
        chats: [],
        tasks: [],
        chat: {
          lang: 'russian'
        }
      },
      options))
  }

  request(method, params, data) {
    let url = {
      method: isObject(data) ? 'POST' : 'GET',
      hostname: 'api.telegram.org',
      path: `/bot${this.token}/${method}`
    }
    if (data) {
      data = Buffer.from(JSON.stringify(data), 'utf8')
      url.headers = {
        'content-type': 'application/json',
        'content-length': data.byteLength
      }
    }
    if (isObject(params)) {
      url.path += '?' + stringify(params)
    }
    return new Promise(function (resolve, reject) {
      const req = request(url, function (res) {
        const chunks = []
        res.on('data', function (chunk) {
          chunks.push(chunk)
        })
        res.on('end', function () {
          const data = JSON.parse((1 === chunks.length ? chunks[0] : Buffer.concat(chunks)).toString())
          if (data.ok) {
            resolve(data.result)
          }
          else {
            reject(data)
          }
        })
      })
      req.on('error', reject)
      if (isObject(data)) {
        req.write(data)
      }
      req.end()
    })
  }

  getChat(id) {
    if (!this.chats[id]) {
      this.chats[id] = Object.create(this.chat)
    }
    return this.chats[id]
  }

  translate({chat_id, text: {code, params}}) {
    let string = this.thesaurus[this.getChat(chat_id).lang][code]
    each(params, function (v, k) {
      string = string.replace('$' + k, v)
    })
    return string
  }

  getUpdates() {
    return this.request('getUpdates', pick(this, 'timeout', 'offset'))
      .then(({result}) => {
        this.offset = 1 + result.reduce((a, b) => Math.max(a.update_id, b.update_id))
        return result
      })
  }

  getChatMember(chat_id, user_id) {
    return this.request('getChatMember', {chat_id, user_id})
  }

  schedule(options) {
    options.tasks = options.tasks.map((task) => {
      if (task instanceof Array) {
        return () => {
          return Promise.resolve(this[task[0]].apply(this, task.slice(1)))
        }
      }
      return task
    })

    return new Promise((resolve, reject) => {
      const job = () => {
        multi(options)
          .then(() => {
            resolve()
            const next = this.tasks.shift()
            if (next) {
              next()
            }
          })
          .catch(reject)
      }

      if (this.tasks.length > 0) {
        this.tasks.push(job)
      }
      else {
        job()
      }
    })
  }

  infiniteUpdate(cb) {
    return this.getUpdates().then(m => m.map(s => cb(s.message)))
  }

  sendMessage(options) {
    if (isObject(options.text)) {
      options.text = this.translate(options.text)
    }
    return this.request('sendMessage', null, options)
  }
}

module.exports = {Bot}
