/*
 const {createServer} = require('https')
 const pem = require('pem')
 const fs = require('fs')

 function serve(req, res) {
 res.end('Hello')
 }

 pem.createCertificate({days: 1, selfSigned: true}, function (err, keys) {
 if (err) {
 console.error(err)
 }
 else {
 const server = createServer({key: keys.serviceKey, cert: keys.certificate}, serve)
 server.listen(54321, '0.0.0.0')
 }
 })
 */
// 52.57.80.89

const http = require('http')
const https = require('https')
const {parse} = require('url')

const parsable = ['text/html', 'text/css', 'application/javascript']
const cache = {}
const secure = new Set()

const server = http.createServer(function (req, res) {
  if ('microsoft.com' === req.headers.host) {
    return res.end(JSON.stringify({
      secure: Array.from(secure),
      cache: Object.keys(cache)
    }))
  }
  try {
    const url = parse(req.url)
    const options = {
      method: req.method,
      host: req.headers.host,
      path: url.path,
      headers: req.headers
    }
    delete options.headers['accept-encoding']
    options.headers['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0'
    if (options.path.indexOf('?') < 0 && req.url in cache) {
      const c = cache[req.url]
      res.writeHead(c.statusCode, c.headers)
      res.end(c.body)
      return
    }
    function request(options) {
      const creq = (secure.has(options.host) ? https : http).request(options, function (cres) {
        if (cres.headers.location && req.url === cres.headers.location.replace(/https/, 'http')) {
          secure.add(options.host)
          return request(options)
        }
        res.writeHead(cres.statusCode, res.headers)
        if (cres.statusCode < 300 || cres.statusCode >= 400) {
          const type = /\w+\/\w+/.exec(cres.headers['content-type'])
          const charset = /charset=(.*)/.exec(cres.headers['content-type'])
          const ctype = parsable.indexOf(type ? type[0] : 'text/html')
          if (ctype >= 0 && (!charset || 'utf-8' === charset[1])) {
            const chunks = []
            cres.on('data', function (chunk) {
              chunks.push(chunk)
            })
            cres.on('end', function () {
              let string = (1 === chunks.length ? chunks[0] : Buffer.concat(chunks)).toString('utf8')
              string = string.replace(/https:\/\/([\w\-.]+)/g, function (s) {
                secure.add(s[1])
                return s[0]
              })
              res.end(string, function () {
                if (ctype > 0) {
                  cache[req.url] = {
                    statusCode: cres.statusCode,
                    headers: cres.headers,
                    body: string
                  }
                }
              })
            })
          }
          else {
            cres.pipe(res)
          }
        }
        else {
          res.end()
        }
      })
      req.pipe(creq)
    }

    request(options)
  }
  catch (ex) {
    res.writeHead(500, {
      'content-type': 'application/json'
    })
    res.end(JSON.stringify(ex))
  }
})

server.listen(54321)
