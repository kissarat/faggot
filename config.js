const merge = require('deepmerge')
const fs = require('fs-extra')
const {resolve} = require('path')
const argv = require('optimist')
  .default('settings', './local')
  .argv

var local
try {
  local = require(argv.settings)
}
catch (ex) {
  local = false
  console.warn('No local config')
}

const config = {
  autosave: 60,
  secret: "cheilohdoh6ohTaizeiyokichee7ookeis4zei1Kaecaikai",
  port: 16666,
  threads: 100,
  chat_id: -1001050241147,
  database: {
    user: 'faggot',
    password: 'faggot',
    database: 'faggot'
  }
}

const settings = local ? merge(local, config) : config

function autosave() {
  function extract(node, local) {
    const result = {}
    for (const key in local) {
      const value = local[key]
      result[key] = value && 'object' === typeof value ? extract(node[key], value) : node[key]
    }
    return result
  }

  fs.writeJson(argv.settings, extract(settings, local), function (err) {
    if (err) {
      console.error(err)
    }
  })
}

if (settings.autosave) {
  argv.settings = resolve(__dirname + '/' + argv.settings + '.json')
  console.log(`Automatic save local configuration to ${argv.settings} every ${config.autosave} seconds`)
  setInterval(autosave, config.autosave * 1000)
}

module.exports = settings
