const config = require('./config')
const fs = require('fs')
const {sample, uniq, zip, each} = require('underscore')

const Bot = require('./telegram')
const bot = new Bot(config)

const anwsers = fs
  .readFileSync(__dirname + '/answers.txt')
  .toString('utf8')
  .split('\n')
  .filter(s => s.trim())

const sentences = require('./strings-classified')
sentences.russian = uniq(anwsers.concat(sentences.russian))

// const pool = new Pool(config.database)


function error(err) {
  console.error(err)
}

const languages = [
  'Chinese',
  'English',
  'German',
  'Indonesian',
  'Korean',
  'Portuguese',
  'Russian',
  'Spanish'
]

const stateDictionary = {
  lang: {}
}

languages.forEach(function (lang) {
  stateDictionary.lang[lang] = lang.toLowerCase()
})

bot.infiniteUpdate(function (m) {
  const chat = bot.getChat(m.chat_id)
  each(stateDictionary, function (v, k) {
    if (v[m.text.toLowerCase()]) {
      chat[k] = v[m.text]
    }
  })

  console.log(chat.lang, sample(sentences[chat.lang]), m.text)
  bot.sendMessage({
    chat_id: m.chat.id,
    text: sample(sentences[chat.lang]),
    reply_markup: {
      keyboard: [languages],
      one_time_keyboard: false,
      resize_keyboard: true
    }
  })
})
  .catch(error)

module.exports = bot
