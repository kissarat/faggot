const config = require('./config')
const http = require('http')
const https = require('https')
const {compileFile} = require('pug')
const {omit} = require('underscore')
const {Pool} = require('pg')
const {readJsonSync, readFile} = require('fs-extra')
const {resolve} = require('path')

const pool = new Pool(config.database)

function template(name, variables) {
  if (!template.cache) {
    template.cache = {}
  }
  if (!template.cache[name]) {
    template.cache[name] = compileFile(resolve(__dirname + `/template/${name}.pug`), {
      basedir: resolve(__dirname + '/template'),
      pretty: true
    })
  }
  return template.cache[name](variables)
}

function receive(req, cb) {
  const chunks = []
  req.on('data', function (chunk) {
    chunks.push(chunk)
  })
  req.on('end', function () {
    const data = JSON.parse((1 === chunks.length ? chunks[0] : Buffer.concat(chunks)).toString())
    cb(data)
  })
}

const secret_url = '/' + config.secret

function saveMessage(m, cb) {
  pool.query('insert into message(chat, id, data) values ($1, $2::BIGINT, $3::JSON) returning id',
    [m.chat, m.id, omit(m, 'id')],
    function (err, result) {
      if (err) {
        console.error(err.code)
      }
      cb(err, result)
    })
}

const server = http.createServer(function (req, res) {
  function answer(code, headers, body) {
    if ('string' === typeof headers) {
      headers = {'content-type': headers}
    }
    res.writeHead(code, headers)
    res.end(body)
  }

  function json(code, object) {
    answer(code, {'content-type': 'application/json'}, JSON.stringify(object, null, '  '))
  }

  function sendHTML(name, variables) {
    answer(200, 'text/html; charset=utf-8', template(name, variables))
  }

  const messageMatch = /^.message.(\w+).(\w+)/.exec(req.url)

  if (0 === req.url.indexOf(secret_url)) {
    const name = req.url.slice(secret_url.length).slice(1)
    if ('GET' === req.method) {
      if (req.url.length > secret_url.length) {
        json(200, readJsonSync(__dirname + '/' + name))
      }
      else {
        pool.query('select * from member', function (err, result) {
          if (err) {
            json(500, err)
          }
          else {
            answer(200, 'text/html; charset=utf-8', generate({
              rows: result.rows
            }))
          }
        })
      }
    }
    else {
      receive(req, function (m) {
        m.chat = name
        saveMessage(m, function () {
          res.end()
        })
      })
    }
  }
  else if (messageMatch) {
    pool.query('SELECT * FROM message WHERE chat = $1 AND id = $2::BIGINT', messageMatch.slice(1), function (err, result) {
      if (err) {
        json(500, err)
      }
      else if (1 === result.rows.length) {
        const message = result.rows[0]
        message.previous = message.id - 1
        message.next = +message.id + 1
        message.data = JSON.stringify(message.data, null, '    ')
        sendHTML('message', message)
      }
      else {
        sendHTML('message', {id: messageMatch[1]})
      }
    })
  }
  else if ('/script.js' === req.url) {
    readFile(__dirname + '/script.js', function (err, data) {
      if (err) {
        json(500, err)
      }
      else {
        answer(200, {['content-type']: 'application/javascript'}, data)
      }
    })
  }
  else if ('/favicon.ico' === req.url) {
    answer(301, {location: 'https://help.ubuntu.com/favicon.ico'})
  }
  else {
    json(405, {error: {message: 'Unknown route'}})
  }
})

server.listen(config.port)
