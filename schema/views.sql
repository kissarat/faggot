CREATE OR REPLACE VIEW history AS
  WITH t AS (
      SELECT
        id,
        chat,
        cast("data" ->> 'message' AS TEXT) AS message,
        replace((to_timestamp(("data" ->> 'date') :: TEXT :: BIGINT) :: TIMESTAMP WITH TIME ZONE) :: TEXT, '+03',
                '')                        AS time
      FROM "message"
  )
  SELECT *
  FROM t
  WHERE message IS NOT NULL AND message <> ''
  ORDER BY id DESC;

CREATE OR REPLACE VIEW pre_bot_history AS
  WITH t AS (
      SELECT
        id,
        cast("data" ->> 'fromID' AS TEXT) :: BIGINT AS "from",
        cast("data" ->> 'message' AS TEXT)          AS message,
        replace((to_timestamp(("data" ->> 'date') :: TEXT :: BIGINT) :: TIMESTAMP WITH TIME ZONE) :: TEXT, '+03',
                '')                                 AS time
      FROM "bot_message"
  )
  SELECT *
  FROM t
  WHERE message IS NOT NULL AND message <> ''
  ORDER BY id DESC;

CREATE MATERIALIZED VIEW from_id AS
  WITH
      s1 AS (
        SELECT cast("data" ->> 'from_id' AS TEXT) :: BIGINT AS "from"
        FROM "message"
    ),
      s2 AS (
        SELECT
          "from",
          count(*) AS "count"
        FROM s1
        GROUP BY "from")
  SELECT
    "from",
    count
  FROM s2
  ORDER BY count DESC;

CREATE MATERIALIZED VIEW new_chat_member AS
  SELECT
    chat,
    id,
    cast(data -> 'new_chat_member' ->> 'username' AS TEXT) AS "username"
  FROM message
  WHERE data -> 'new_chat_member' ->> 'username' IS NOT NULL
  ORDER BY id;

CREATE MATERIALIZED VIEW member_nick AS
  SELECT nick
  FROM (SELECT substring(message FROM '\s*@([^\s]+)\s*') AS nick
        FROM history) t
  GROUP BY nick
  ORDER BY nick;
