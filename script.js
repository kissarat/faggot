const idRegex = /\d+$/
let id = idRegex.exec(location.pathname)

if (id) {
  id = +id[0]
  addEventListener('keyup', function (e) {
    switch (e.key) {
      case 'ArrowRight':
        location.pathname = location.pathname.replace(idRegex, id + 1)
        break;
      case 'ArrowLeft':
        location.pathname = location.pathname.replace(idRegex, id - 1)
        break;
    }
  })
}
