const {Pool} = require('pg')
const {Bot} = require('./telegram')
const config = require('./config')

const bot = new Bot(config)
const pool = new Pool(config.database)

const UPDATE = `UPDATE member
SET username = $2, last_name = $3, first_name = $4, status = $5, time = CURRENT_TIMESTAMP
WHERE id = $1`

pool.query('SELECT * FROM member WHERE time IS NULL ORDER BY count DESC, id ASC', function (err, result) {
  console.error(err)
  bot.schedule({
    threads: 4,
    delay: 600,
    tasks: result.rows.map(m => ['getChatMember', -1001050241147, m.id]),
    success(result) {
      const u = result.user
      pool.query(UPDATE, [u.id, u.username, u.last_name, u.first_name, result.status], function (err) {
        console.error(err)
      })
    },
    error(err) {
      console.error(err)
    }
  })
})
