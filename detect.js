const detector = new (require('languagedetect'))()
const strings = require('./strings.json')
const fs = require('fs')
const {each} = require('underscore')

const alpha = {
  chinese: '上下不与且世丝个中丰为主么乐也习了事互些交享人什仅今介从他付令以们价任伙会伴伽但位住何余作你佣使供侣便候值做入全关兴其写决况准几凭出击分切划列创到制力功动助努劳包区千单即原参友反发取受只叫可合同后向吗吧启吸告员周味和啊喜嗯回因团在地堂处备多够大天奖女好如妹姐姓子字存学它安完定实密富对将小就展差已希帮常年并幸广建开弃弄式引弯当得必怎思总您情惭想意感愧懂成我或所打扬把抑抓投拉拥择括持按挣探接控提插操支收改放数文斗料新方无日时明易是晕曲更最有朋望来果栓样案楚欢欲款正此每气永求没法泛活流深清点热然照牛现理瑜用由申电界留白的直相看着知研确礼社种秘究空立站答筹简类系索线细绍经给统继续网置老考者而能自节花获菜藏螺行表要见观角解言计订认讨让议论识诉试话该语说请谁谈谢账购资起趣跃跟身转轻输边迅过迎运还这进远送适选通速造道邀那都里量金钮钱门闭问间队限随隐集需非面项顺须题额馈验高'.split(''),
  korean: '가각간갈감갑강같개거건걸겁것게겠겨격견결경계고곡곧공과관교구국군굴그글금기긴깊까꺼께끄끝나난날남내낼냈냥너넓넷녀노녹논농누뉴느는늘능니다단담답당대더데도돈돌동돼되될됩두둘드든들디때떤똑뚱라락람랑래러런렇려력령로론르른를름리릭린릴마만말맘매머메면명모목못무문물뭘뮤미믿밀바박발방배백버벌법변별보본볼봅봇부분불비뻐사삼상생서선설성세셔소속솔송수스슨습시신실심써쎄쓰씨아안않알암았액야약어언엄업없엇었에여연열영예오올와외요용우움웃워원위유으은을음응의이인일임입있자작잔잖잘장재저적전젊접정제젠조좀좋좌죠주준줄중즈즉지직진질집쪽찬참찾채처척천철청쳐초최출충친커켜코클택터통투트튼티파팔팬폐프피필하한할함합항해했행향험현호화확환황효흠히'.split('')
}

function abc(string) {
  if (string instanceof Array) {
    string = string.join('')
  }
  const chars = _.uniq(string.split('').map(s => s.charCodeAt(0)).filter(n => n > 128))
  return chars.sort().map(s => String.fromCharCode(s)).join('')
}

function hieroglyphCount(lang, string) {
  let count = 0
  lang = alpha[lang]
  string.split('').forEach(function (c) {
    if (lang.indexOf(c) >= 0) {
      count ++
    }
  })
  return count
}

const available = [
  'chinese',
  'english',
  'german',
  'indonesian',
  'korean',
  'portuguese',
  'russian',
  'spanish'
]

const detected = {_:[]}
available.forEach(s => detected[s] = [])

strings.forEach(function (s) {
  const d = detector.detect(s)
  const lang = d.filter(l => available.indexOf(l[0]) >= 0)[0]
  if (lang) {
    detected[lang[0]].push(s)
  }
  else {
    const chinese = hieroglyphCount('chinese', s)
    const korean = hieroglyphCount('korean', s)
    if (chinese > 0 || korean > 0) {
      detected[korean > chinese ? 'korean' : 'chinese'].push(s)
    }
    else {
      detected._.push(s)
    }
  }
})

each(detected, function (array) {
  array.sort()
})

// available.forEach(function (lang) {
//   fs.writeFileSync(__dirname + `/languages/${lang}.json`, JSON.stringify(detected[lang], null, '  '))
// })

console.log(JSON.stringify(detected, null, '  '))
